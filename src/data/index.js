export const classes = [
  {
    name: 'Economy class',
    id: 'economy_class'
  },
  {
    name: 'Business class',
    id: 'business_class'
  },
  {
    name: 'First class',
    id: 'first_class'
  }
]

export const flightTypes = [
  {
    name: 'Roundtrip',
    id: 'roundtrip'
  },
  {
    name: 'One way',
    id: 'one_way'
  }
]

export const airports = [
  {
    id: 1,
    name: 'Anaa Airport',
    lat: '-17.3595',
    lon: '-145.494',
    code: 'AAA',
    city: 'Anaa',
    country: 'French Polynesia'
  },
  {
    id: 2,
    name: 'El Mellah Airport',
    lat: '36.8236',
    lon: '7.8103',
    code: 'AAE',
    city: 'El Tarf',
    country: 'Algeria'
  },
  {
    id: 3,
    name: 'Aalborg Airport',
    lat: '57.0952',
    lon: '9.85606',
    code: 'AAL',
    city: 'Norresundby',
    country: 'Denmark'
  },
  {
    id: 4,
    name: 'Tirstrup Airport',
    lat: '56.3088',
    lon: '10.6154',
    code: 'AAR',
    city: 'Kolind',
    country: 'Denmark'
  },
  {
    id: 5,
    name: 'Altay Airport',
    lat: '47.7406',
    lon: '88.0845',
    code: 'AAT',
    city: 'Altay',
    country: 'China'
  }
]
